// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.6.10/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyAFX5-RSf8sAQWASJOSSpp919VQDlvW7dY",
    authDomain: "tic-tac-toe-407fc.firebaseapp.com",
    databaseURL: "https://tic-tac-toe-407fc-default-rtdb.firebaseio.com",
    projectId: "tic-tac-toe-407fc",
    storageBucket: "tic-tac-toe-407fc.appspot.com",
    messagingSenderId: "387521726854",
    appId: "1:387521726854:web:9d708d3769159b0e518407",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// import

import {
    getDatabase,
    ref,
    set,
    get,
    child,
    update,
    remove,
} from "https://www.gstatic.com/firebasejs/9.6.10/firebase-database.js";

const db = getDatabase();

let Loadbutton = document.querySelector(".btn-load");

Loadbutton.addEventListener("click", function() {
    const verifyId = document.getElementById("load").value;
    localStorage.setItem("playerTwo", true);
    localStorage.setItem("playerOne", false);
    const dbref = ref(db);

    get(child(dbref, verifyId + "/playerNames"))
        .then((snapshot) => {
            if (snapshot.exists()) {
                localStorage.setItem("id", verifyId);
                window.location.href = "game.html";
            } else {
                alert("no data found");
            }
        })
        .catch((error) => {
            alert("Unsuccessfull, error: " + error);
        });
});