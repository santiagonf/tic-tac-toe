// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.6.10/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyAFX5-RSf8sAQWASJOSSpp919VQDlvW7dY",
    authDomain: "tic-tac-toe-407fc.firebaseapp.com",
    databaseURL: "https://tic-tac-toe-407fc-default-rtdb.firebaseio.com",
    projectId: "tic-tac-toe-407fc",
    storageBucket: "tic-tac-toe-407fc.appspot.com",
    messagingSenderId: "387521726854",
    appId: "1:387521726854:web:9d708d3769159b0e518407",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// import

import {
    getDatabase,
    ref,
    set,
    get,
    child,
    update,
    remove,
} from "https://www.gstatic.com/firebasejs/9.6.10/firebase-database.js";

const db = getDatabase();

// constantes

const STATUS_DISPLAY = document.querySelector(".notification"), // revisa el turno del jugador y notifica el ganador
    GAME_STATE = ["", "", "", "", "", "", "", "", ""], // etsado de las casillas
    WINNINGS = [
        // posibles formas de ganar
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ],
    WIN_MESSAGE = () => `El jugador ${turn} ha ganado!`, // notifica el ganador
    DRAW_MESSAGE = () => `El juego ha terminado en empate!`, // notifica empate
    CURRENT_PLAYER_TURN = () => `Turno del jugador ${turn}`; // notifica el turno actual del jugador

// variables
let nInterval = false;
let gameActive = true;
let currentPlayer = "O";
let turn = "X";

// funciones

function main() {
    // funcion main
    handleStatusDisplay(CURRENT_PLAYER_TURN());
    listeners();
}

function listeners() {
    document
        .querySelector(".game-container")
        .addEventListener("click", handleCellClick); // listener de las casillas
    document
        .querySelector(".restart")
        .addEventListener("click", handleRestartGame); // listener del boton restart
}

function handleStatusDisplay(message) {
    // cambia el mensaje dependiendo del turno del jugador
    STATUS_DISPLAY.innerHTML = message; // imprime el jugador actual o si hay ganador
}

function handleRestartGame() {
    gameActive = true; // le da un status al juego para confirmar si esta en curso la partida o ya termino
    currentPlayer = "X"; // coloca como default X como la figura inicial
    handleStatusDisplay(CURRENT_PLAYER_TURN());
    document
        .querySelectorAll(".game-cell")
        .forEach((cell) => (cell.innerHTML = "")); // coloca las celdas vacias con innerHTML
}

function handleCellClick(clickedCellEvent /** Type Event **/ ) {
    const clickedCell = clickedCellEvent.target; // identifica la casilla clickeada
    if (clickedCell.classList.contains("game-cell")) {
        // si la celda hace parte de game cell, devuelve true
        const clickedCellIndex = Array.from(
            clickedCell.parentNode.children
        ).indexOf(clickedCell);
        if (GAME_STATE[clickedCellIndex] !== "" || !gameActive) {
            // si el estado de juego coincide con algun index (ganador o empate) O el estado del juego es inactivo devuelve falso
            return false;
        }

        handleCellPlayed(clickedCell, clickedCellIndex);
        handleResultValidation();
    }
}

function handleCellPlayed(clickedCell /** object HTML **/ , clickedCellIndex) {
    GAME_STATE[clickedCellIndex] = currentPlayer; // Agrgega en la posición correspondiente el valor ya sea "X" u "O" en el estado actual del juego
    clickedCell.innerHTML = currentPlayer; // Agrega en el HTML el valor del jugador

    turn = turn === "X" ? "O" : "X"; // itera entre la posición correspondiente
    insertData2();

    gameActive = false;
}

function handledGetCellPlayed() {
    for (let i = 0; i < 8; i++) {
        handleCellPlayed(i);
    }
}

function handleResultValidation() {
    let roundWon = false;
    for (let i = 0; i < WINNINGS.length; i++) {
        // Itera cada uno de las posibles combinaciones ganadores
        const winCondition = WINNINGS[i]; // Guarda la combinación por ejemplo: [0, 1, 2]
        let position1 = GAME_STATE[winCondition[0]], // vacio, "x" u "o"
            position2 = GAME_STATE[winCondition[1]],
            position3 = GAME_STATE[winCondition[2]]; // Almacena el valor del estado actual del juego según las posiciones de winCondition

        if (position1 === "" || position2 === "" || position3 === "") {
            continue; // Si hay algún valor vacio nadie ha ganado aún
        }
        if (position1 === position2 && position2 === position3) {
            roundWon = true; // Si todas las posiciones coinciden entonces, dicho jugador ha ganado la partida
            break;
        }
    }

    if (roundWon) {
        turn = turn === "X" ? "O" : "X";
        handleStatusDisplay(WIN_MESSAGE());
        gameActive = false;
        clearInterval(nInterval);
        document.querySelector(".restart").hidden = false;
        return;
    }

    let roundDraw = !GAME_STATE.includes(""); // Si todas las celdas tienen valor y la sentencia anterior fue falsa entonces es empate
    if (roundDraw) {
        handleStatusDisplay(DRAW_MESSAGE());
        gameActive = false;
        return;
    }

    handlePlayerChange();
}

function handlePlayerChange() {
    // cambia entre los jugadores para asegurar que no repita jugada
    if (localStorage.getItem("playerOne") === "true") {
        currentPlayer = "X";
    } else {
        currentPlayer = "O";
    }

    handleStatusDisplay(CURRENT_PLAYER_TURN());
}

function hideBtn() {
    // oculta el boton hasta que el juego termine
    document.querySelector(".restart").hidden = true;
}

hideBtn();

function restartGameState() {
    // funcion para resetear el juego
    let i = GAME_STATE.length;

    while (i--) {
        GAME_STATE[i] = "";
        setPosition();
    }
    insertData2();
}

main();

// jugadores

// visualize the game ID to be shared
function getId() {
    let idGame = localStorage.getItem("id");
    document.getElementById("gameId").innerHTML = `Your game id is: ${idGame}`;
    return idGame;
}

getId();

// funcion para guardar informacion en FireBase
let player1Name = document.getElementById("player1-name");

let saveName1 = document.getElementById("save-name1");

let player2Name = document.getElementById("player2-name");

let saveName2 = document.getElementById("save-name2");

let pos0 = document.getElementById("cell0");
let pos1 = document.getElementById("cell1");
let pos2 = document.getElementById("cell2");
let pos3 = document.getElementById("cell3");
let pos4 = document.getElementById("cell4");
let pos5 = document.getElementById("cell5");
let pos6 = document.getElementById("cell6");
let pos7 = document.getElementById("cell7");
let pos8 = document.getElementById("cell8");

function insertData2(data) {
    player1Name = document.getElementById("player1-name");
    player2Name = document.getElementById("player2-name");
    pos0.value = GAME_STATE[0];
    pos1.value = GAME_STATE[1];
    pos2.value = GAME_STATE[2];
    pos3.value = GAME_STATE[3];
    pos4.value = GAME_STATE[4];
    pos5.value = GAME_STATE[5];
    pos6.value = GAME_STATE[6];
    pos7.value = GAME_STATE[7];
    pos8.value = GAME_STATE[8];

    set(ref(db, getId() + "/playerNames"), {
            player1: player1Name.value,
            player2: player2Name.value,
            pos0: pos0.value,
            pos1: pos1.value,
            pos2: pos2.value,
            pos3: pos3.value,
            pos4: pos4.value,
            pos5: pos5.value,
            pos6: pos6.value,
            pos7: pos7.value,
            pos8: pos8.value,
            turn: turn,
        })
        .then(() => {
            console.log("data stored successfully");
        })
        .catch((error) => {
            alert("error, error: " + error);
        });
    nInterval = true;

    consultaNametro();
}

saveName2.addEventListener("click", insertData2);
saveName1.addEventListener("click", insertData2);

// funcion para pedir informacion en FireBase
function selectData() {
    const dbref = ref(db);

    get(child(dbref, getId() + "/playerNames"))
        .then((snapshot) => {
            if (snapshot.exists()) {
                player1Name.value = snapshot.val().player1;
                player2Name.value = snapshot.val().player2;
                GAME_STATE[0] = snapshot.val().pos0;
                GAME_STATE[1] = snapshot.val().pos1;
                GAME_STATE[2] = snapshot.val().pos2;
                GAME_STATE[3] = snapshot.val().pos3;
                GAME_STATE[4] = snapshot.val().pos4;
                GAME_STATE[5] = snapshot.val().pos5;
                GAME_STATE[6] = snapshot.val().pos6;
                GAME_STATE[7] = snapshot.val().pos7;
                GAME_STATE[8] = snapshot.val().pos8;
                turn = snapshot.val().turn;
            } else {
                alert("no data found");
            }
        })
        .catch((error) => {
            alert("Unsuccessfull, error: " + error);
        });

    nInterval = true;
    setPosition();
    turns();
    handleResultValidation();
}

function turns() {
    // asigna turnos para evitar que el otro jugador pueda colocar una figura antes que el jugador actual termine su turno
    gameActive = false;

    if (turn === "X" && localStorage.getItem("playerOne") === "true") {
        gameActive = true;
    }
    if (turn === "O" && localStorage.getItem("playerTwo") === "true") {
        gameActive = true;
    }
}

function setPosition() {
    pos0.innerHTML = GAME_STATE[0];
    pos1.innerHTML = GAME_STATE[1];
    pos2.innerHTML = GAME_STATE[2];
    pos3.innerHTML = GAME_STATE[3];
    pos4.innerHTML = GAME_STATE[4];
    pos5.innerHTML = GAME_STATE[5];
    pos6.innerHTML = GAME_STATE[6];
    pos7.innerHTML = GAME_STATE[7];
    pos8.innerHTML = GAME_STATE[8];
}

function restrictionPlayers() {
    // evita que los jugadores peudan cambiar el nombre de su contrincante
    let numeroDeJugadores = localStorage.getItem("numeroDeJugadores");
    if (localStorage.getItem("playerOne") === true) {
        document.getElementById("player2-name").disabled = true;
        document.querySelector("#save-name2").disabled = true;
    }

    if (localStorage.getItem("playerTwo") === "true") {
        document.getElementById("player2-name").disabled = false;
        document.getElementById("player1-name").disabled = true;

        document.getElementById("save-name1").disabled = true;

        gameActive = false;

        selectData();

        let game = true;
        while (game) {
            // cuando llegue al final del juego o alguien gane, game pasa a false
            console.log("fin del juego");
            game = false;
        }
    }

    function nombreJugador1() {
        if (numeroDeJugadores === "2") {
            const nombreJugador1 = document.getElementById("player1-name"); //.innerHTML = localStorage.getItem('name1')

            nombreJugador1.innerHTML = localStorage.getItem("name1");
        }
    }
}

// cosulta constante

function consultaNametro() {
    if (nInterval) {
        nInterval = setInterval(selectData, 5000);
    }
}

consultaNametro();
restrictionPlayers();