// posibles formas de ganar
// [0,1,2]
// [3,4,5]
// [6,7,8]
// [0,3,6]
// [1,4,7]
// [2,5,8]
// [0,4,8]
// [2,4,6]

// generate the UUID when the button is clicked

const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

function generateString(length) {
    let result = " ";
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

function startGame() {
    let idGame = generateString(4);

    localStorage.setItem("id", idGame.trim());

    localStorage.setItem("numeroDeJugadores", 1);

    localStorage.setItem("playerOne", true);

    localStorage.setItem("playerTwo", false);

    window.location.href = "/game.html";
}

// const newGameCode = document
//     .querySelector(".new-game")
//     .addEventListener("click", function() {
//         let idGame = generateString(4);

//         localStorage.setItem("id", idGame);

//         localStorage.setItem("numeroDeJugadores", 1);
//     });

// editable space for players names

// var newlabel = document.createElement("Label");
// newlabel.setAttribute("for", id_from_input);
// newlabel.innerHTML = "Here goes the text";
// parentDiv.appendChild(newlabel);