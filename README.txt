// read.me

El juego comienza con el boton "New game", en el momento se genera un Id unico por partida que permite que al ser compartido
otra persona se pueda unir a la partida mediante el boton "Load Game".

Cuando el jugador 1 entre, antes de jugar debe colocar su nombre y clickear el boton "save" al lado de su campo de texto,
lo mismo debe hacer el jugador 2.

Una vez que ambos jugadores registraron su nombre el juego puede comenzar, ambos jugadores se pueden dar cuenta que no 
pueden modificar el nombre del contricante, no pueden jugar 2 veces antes de que el turno del contricante termine, 
no pueden sobreescribir la casilla propia o del contrincante y solo se puede resetear el juego una vez haya un ganador
o se considere un empate el juego.

importante:
- el juego solo comenzara hasta que ambos jugadores escriban su nombre
- el juego termina solo hastq ue haya un ganador o haya un empate